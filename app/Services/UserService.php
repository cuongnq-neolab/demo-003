<?php

namespace App\Services;

use App\Repositories\UserRepository;
// use Carbon\Carbon;


class UserService
{

    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
    }

    public function getAll()
    {
        return $this->userRepository->orderBy('created_at', 'desc')->all();
    }

    public function getById($id)
    {
        return $this->userRepository->find($id);
    }

    public function getCounts()
    {
        return $this->getAll()->count();
    }

    public function insert($params)
    {
        return $this->userRepository->create($params);
    }

    public function delete($id)
    {
        return $this->getById($id)->delete();
    }

    public function update($id)
    {
        return $this->getById($id)->delete();
    }

    
}
