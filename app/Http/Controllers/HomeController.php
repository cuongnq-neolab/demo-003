<?php

namespace App\Http\Controllers;

use App\Entities\User;
use App\Services\UserService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $userService;

    public function __construct()
    {
        $this->userService = app(UserService::class);
    }

    public function index()
    {
        $allUsers = $this->userService->getAll();
        return view('home', ['users' => $allUsers]);
    }
}
