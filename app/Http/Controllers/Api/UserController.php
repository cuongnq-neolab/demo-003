<?php

namespace App\Http\Controllers\Api;

use App\Entities\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\Api\RegisterUser;
use App\Services\UserService;

use App\Http\Controllers\Api\ApiController;

class UserController extends ApiController
{

    protected $userService;

    public function __construct()
    {
        sleep(1);
        // $path = storage_path('app/file.txt');
        // $storagePath = Storage::disk('s3')->put("uploads", $path, 'public');

        $this->userService = app(UserService::class);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = $this->userService->getAll();
        return $this->success($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return 123;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RegisterUser $request)
    {
        // var_dump($request->all());
        
        $inserted = $this->userService->insert($request->all());
        if($inserted->id)
        {
            return $this->success(['user_id' => $inserted->id]);
        }
        return $this->error('Can not create member', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return $user;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $user = $this->userService->getById($id);
        if($user) {
            return $this->success(['user' => $user]);
        }
        return $this->error('Not found this id', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $data = $request->all();
        unset($data['id']);
        $updated = $this->userService->getById($id)->update($data);
        return $this->success([
            'deleted' => $updated
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $deleted = $this->userService->delete($id);
        return $this->success([
            'deleted' => $deleted,
            'count' => $this->userService->getCounts()
        ]);
    }
}
