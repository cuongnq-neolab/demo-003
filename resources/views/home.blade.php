<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<meta name="generator" content="">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>Admin</title>
		<!-- app core CSS -->
		<link href="/css/app.css" rel="stylesheet">
	</head>
	<body class="bg-light">
		<div class="container" id="app">
		
			<div class="py-5 text-center">
				<h2>Sample form</h2>
				<p class="lead"></p>
			</div>
			<div class="row">
				<div class="col-md-4 order-md-2 mb-4">
					<h4 class="d-flex justify-content-between align-items-center mb-3">
						<span class="text-muted">Member</span>
						<span class="badge badge-secondary badge-pill">
							<span id="mem-count-loading" class="spinner-border spinner-border-sm" role="status" aria-hidden="true" style="display: none"></span>
							<span id="mem-count-result" class="label-data">0</span>
						</span>
					</h4>
					<div class="alert alert-warning mb-4" id="data-mem-loading">
						<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...
					</div>
					<div class="mb-3" id="data-mem-result">
						<ul class="list-group list-new"></ul>
					</div>
					
	
				</div>
				<div class="col-md-8 order-md-1">
					<div id="mem-tab-add">
						<div class="d-flex align-items-center">
							<h4 class="mb-3">Add member</h4>
						</div>
					</div>
					<div id="mem-tab-edit" class="row" style="display: none">
						<div class="col-sm-6">
							<h4 class="mb-3">Edit member <span></span></h4>
						</div>
						<div class="col-sm-6 text-right">
							<button id="mem-edit-loading" class="btn btn-primary" type="button" disabled style="display: none">
								<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
							</button>
							<button id="mem-btn-show-form" class="btn btn-success">Add Member</button>
						</div>
					</div>
					<form id="mem-form" class="needs-validation" action="/api/user" method="POST" novalidate>
						<div class="row">
							<div class="col-md-12 mb-3">
								<label for="input_name">Username</label>
								<input type="text" class="form-control" name="name" id="input_name" placeholder="" value="" required>
								<div class="invalid-feedback">
									Valid username is required.
								</div>
							</div>
	
						</div>

						<div class="mb-3">
							<label for="email">Email</label>
							<input type="email" class="form-control" name="email" id="input_email" placeholder="you@example.com" required>
							<div class="invalid-feedback">
								Please enter a valid email address.
							</div>
						</div>
						<div class="mb-3">
							<label for="level">Level</label>
							<input type="text" class="form-control" id="input_level" name="level" placeholder="1, 2, 3, 4..." required>
							<div class="invalid-feedback">
								Please enter level
							</div>
						</div>
						<div class="mb-3">
							<label for="description">Description</label>
							<input type="text" class="form-control" id="input_description" name="description" placeholder="Description bla bla">
							<div class="invalid-feedback">
								Please enter Description
							</div>
						</div>
						
						<div class="mb-3" id="global-alert" style="display:none">
							<div class="alert alert-info">
								<span></span>
							</div>
						</div>
						<hr class="mb-4">
						<input type="hidden" id="input_id" name="id" value="0">
						<button class="btn btn-warning btn-block" id="mem-form-loading" type="button" style="display: none">
							<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Loading...
						</button>
						<button class="btn btn-primary btn-block" type="submit">
							Save
						</button>
					</form>
				</div>
			</div>
			<footer class="my-5 pt-5 text-muted text-center text-small">
				<p class="mb-1">&copy; 2017-2019 Company Name</p>
				<ul class="list-inline">
					<li class="list-inline-item">
						<a href="#">Privacy</a>
					</li>
					<li class="list-inline-item">
						<a href="#">Terms</a>
					</li>
					<li class="list-inline-item">
						<a href="#">Support</a>
					</li>
				</ul>
			</footer>
		</div>
		<script src="/js/app.js"></script>
	</body>
</html>