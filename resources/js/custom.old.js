function getAllMember()
{
  $userCounts = $('#userCounts');
  $userCounts.find('.spinner-border').show();
  $userCounts.find('.label-data').hide();
  $('#memberLoading').show();
  axios.get('/api/user')
  .then(function (res) {
    let data = res.data;
    if(data.status) {
      $userCounts.find('.label-data').text(data.data.length);

      markup = `
      <ul class="list-group">
          ${data.data.map(user => `<li class="list-group-item d-flex justify-content-between lh-condensed" data-id="${user.id}">
            <div>
              <h6 class="my-0">
                <a href="javascript:void(0)">${user.name}</a>
                - Lv.${user.level}
              </h6>
              <small class="text-muted">${user.description}</small>
            </div>
            <span class="text-muted">
              <button class="btn btn-danger btn-sm">X</button>
            </span>
          </li>`).join('')}
      </ul>
      `;
      
      $('#listMembers').html(markup);
    }
  })
  .catch(function (error) {
    console.log(error);
    alert('Error when load all member');
  })
  .then(function () {
    $userCounts.find('.spinner-border').hide();
    $userCounts.find('.label-data').show();
    $('#memberLoading').hide();
  });
}

$(document).ready(function() {
  getAllMember();
  $globalAlert = $('#globalAlert');
  
  $('#saveMemberForm').submit(function(e) {
    e.preventDefault();
    let params = $(this).serialize();
    $btnLoading = $('#btnLoading');
    $btnSubmit = $(this).find('button[type=submit]');

    $btnSubmit.hide();
    $btnLoading.show();
    axios.post('/api/user', params)
    .then(function (res) {
      data = res.data;
      if(data.status)
      {
        $globalAlert.find('span').text('Create member success!!!');
        $(this).trigger("reset");
        getAllMember();
      }
      else
      {
        $globalAlert.find('span').text(res.message);
      }
      $globalAlert.fadeIn();
    })
    .catch(function (error) {
      errorData = error.response.data.errors;
      $globalAlert.find('span').text(error.response.data.message);
      $globalAlert.fadeIn();
      
    })
    .then(function () {
      $btnSubmit.show();
      $btnLoading.hide();
    });
  });

});

