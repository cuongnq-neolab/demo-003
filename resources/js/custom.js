

let Members = class Members {
  constructor() {
    this.config();
    this.event();
    this.assignEvent();
  }

  config() {
    this.$el = {
      globalAlert: $('#global-alert'),
      countLoading: $('#mem-count-loading'),
      countResult: $('#mem-count-result'),
      memberLoading: $('#data-mem-loading'),
      memberResult: $('#data-mem-result'),
      memberForm: $('#mem-form'),
      memberFormLoading: $('#mem-form-loading'),
      memberFormSubmit: $('#mem-form').find('button[type=submit]'),
      memberEditLoading: $('#mem-edit-loading'),
      memberTabAdd: $('#mem-tab-add'),
      memberTabEdit: $('#mem-tab-edit'),
      memberBtnShowForm: $('#mem-btn-show-form'),

    }
  }

  event() {

    this.eventHandlerSubmit = (e) => {
      e.preventDefault();
      let method, url, msgSuccess;
      let $form = $(e.currentTarget);
      let params = $form.serialize();
      let memberId = $form.find('#input_id').val();
      this.$el.memberFormSubmit.hide();
      this.$el.memberFormLoading.show();
      this.$el.globalAlert.hide();
      // console.log(memberId);
      method = 'post';
      url = '/api/user';
      msgSuccess = 'Create member success!!!';
      if(memberId > 0) {
        method = 'put';
        url += '/' + memberId;
        msgSuccess = 'Update member success!!!';
      }
      this.sendAsync(method, url, params).then((res) => {
        
        let data = res.data;
        if(data.status)
        {
          this.sendAlert('success', msgSuccess)
          $form.trigger("reset");
          this.refreshMembers();
          
        }
        else
        {
          let errors = err.errors;
          const [first] = Object.keys(errors)
    
          this.sendAlert('warning', errors[first]);
        }

      })
      .catch((err) => {
        let errors = err.response.data.errors;
        const [first] = Object.keys(errors)
  
        this.sendAlert('danger', errors[first]);
      })
      .then(() => {
        this.$el.memberFormSubmit.show();
        this.$el.memberFormLoading.hide();
      });
      
    }

    this.eventHandlerMemberReady = (e) => {
      this.refreshMembers();
    }

    this.eventHandlerMemberRemove = (e) => {
      e.preventDefault();
      let $btn = $(e.currentTarget);
      let $parent = $btn.closest('li');

      $btn.attr('disabled', 'disabled');
      $btn.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>');
      this.sendAsync('delete', '/api/user/' + $parent.data('id')).then((res) => {
        let data = res.data.data;
        if(data.deleted) {
          $parent.slideUp(200, () => {
            $parent.remove();
            this.$el.countResult.text(data.count);
          })
        } else {
          alert('Can not delete this user!!!');
          $btn.text('X');
          $btn.removeAttr('disabled');
        }
      }).catch((err) => {
        $btn.text('X');
        $btn.removeAttr('disabled');
      });
    }

    this.eventHandlerMemberEdit = (e) => {
      let $this = $(e.currentTarget);
      let $parent = $this.closest('li');
      let memberId = $parent.data('id');
      this.$el.globalAlert.hide();
      this.$el.memberEditLoading.show();

      this.$el.memberTabAdd.hide();
      this.$el.memberTabEdit.show();
      this.sendAsync('get', '/api/user/' + memberId + '/edit').then((res) => {
        let data = res.data.data;
        if(data.user) {

          this.$el.memberTabEdit.find('h4 span').text(data.user.name);
          $('#input_id').val(memberId);
          $.each(data.user, (key, value) => {
            $('#input_' + key).val(value);
          });

        } else {
          alert(data.msg);
        }
      }).catch((err) => {
        alert(err);
      }).then(() => {
        this.$el.memberEditLoading.hide();
      });
    }

    this.eventHandlerShowForm = (e) => {
      e.preventDefault();
      $('#input_id').val(0);
      this.$el.memberTabAdd.show();
      this.$el.memberTabEdit.hide();
      this.$el.memberForm.trigger("reset");
    }

    this.refreshMembers = () => {
      this.$el.memberLoading.show();
      this.sendAsync('get', '/api/user').then((res) => {
        let data = res.data;
        let users = data.data;
        if(data.status) {
          var html = '';
          this.$el.countResult.text(data.data.length);
          _.forEach(users, function(user) {
            var compiled = _.template('<li class="list-group-item d-flex justify-content-between lh-condensed" data-id="${id}"> <div> <h6 class="my-0"> <a href="javascript:void(0)">${name}</a> - Lv.${level}</h6> <small class="text-muted">${description}</small> </div><span class="text-muted"> <button class="btn btn-danger btn-sm">X</button> </span></li>');
            html += compiled(user);
          });
          
          this.$el.memberResult.find('ul').html(html);
        }
      }).catch((err) => {

      }).then(() => {
        this.$el.memberLoading.hide();
      });
    }

 
    this.sendAsync = async (type, url, params) => {
      let res = await axios({
        method: type,
        url: url,
        data: params
      });
      return res;
    }

    this.sendAlert = (type, msg) => {
      this.$el.globalAlert.hide();
      let alert = this.$el.globalAlert.find('.alert');
      alert.attr('class', 'alert');
      if(type == null) type = 'danger';
      alert.addClass('alert-' + type);
      alert.find('span').text(msg);
      this.$el.globalAlert.show();
    }
  }

  assignEvent() {
    this.$el.memberForm.on('submit', this.eventHandlerSubmit.bind(this));
    this.$el.memberResult.ready(this.eventHandlerMemberReady.bind(this));
    this.$el.memberResult.on('click', '.btn', this.eventHandlerMemberRemove.bind(this));
    this.$el.memberResult.on('click', 'a', this.eventHandlerMemberEdit.bind(this));
    this.$el.memberBtnShowForm.on('click', this.eventHandlerShowForm.bind(this));
  }



}

new Members;